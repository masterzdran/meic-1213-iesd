\select@language {portuguese}
\contentsline {chapter}{\IeC {\'I}ndice}{i}{Doc-Start}
\contentsline {chapter}{\numberline {1}Enquadramento do trabalho}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Resolu\IeC {\c c}\IeC {\~a}o do trabalho}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Segundo trabalho pr\IeC {\'a}tico}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Terceiro trabalho pr\IeC {\'a}tico}{3}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Descri\IeC {\c c}\IeC {\~a}o do trabalho}{3}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Estudo da solu\IeC {\c c}\IeC {\~a}o}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}Poss\IeC {\'\i }veis Melhorias}{4}{section.2.4}
\contentsline {section}{\numberline {2.5}Vantagens VS Desvantagens}{6}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}\emph {Cluster} Hibrido sobre IaaS}{6}{subsection.2.5.1}
\contentsline {chapter}{Bibliografia}{7}{chapter*.5}
